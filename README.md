# Test Assignment Backend Java Serenity

<h2>How to Install</h2>

Clone the repository to your local machine and... that's it<br>
No additional installations are required

<h2>How to Run Tests</h2>

There is 2 options to run tests:

1) Run tests via runner class:
```
com.testassignment.spotify.runner.TestRunner
```
2) Run tests via maven command:
```
mvn clean verify
```

In second case framework generate HTML Serenity report that could be found here:
```
target/site/serenity/index.html
```
Please be aware, running tests via cucumber feature file itself doesn't work due to ~~Serenity crapiness~~ technical limitations :)

<h2>How to Add New Test</h2>

1) Navigate to `resources/features/` directory
2) Open exist `.feature` file or create a new one
3) Add new `Scenario` and describe `Given`, `When`, `Then` steps
4) Add steps definition in one of `StepsDefinition` classes (if required)
5) Add `Service` layer and the rest of underlying technical implementation (if required)
6) Enjoy the result :)

<h2>Known Problems</h2>

1) Console logs require additional treatment
2) Serenity report doesn't include attachments with REST requests and responses
3) Base links and similar are not stored in property file
4) No profiles support for running test on multiple servers
5) No tags support for running specific test suites
