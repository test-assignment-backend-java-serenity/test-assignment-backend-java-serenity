package com.testassignment.spotify.authorize;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class AuthorizeService {

    private final AuthorizeClient authorizeClient = AuthorizeClient.INSTANCE;

    @Step
    public String requestAccessToken() {
        Response response = authorizeClient.postToken();
        String accessToken = response.getBody().path("access_token");

        Serenity.recordReportData().withTitle("Access token").andContents(accessToken);
        return accessToken;
    }
}
