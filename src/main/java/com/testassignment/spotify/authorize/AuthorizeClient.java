package com.testassignment.spotify.authorize;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

/**
 * REST Client for Authorization API
 * Singleton implementation
 */
@Slf4j
enum AuthorizeClient {

    INSTANCE;

    private static final String GRANT_TYPE = "grant_type";
    private static final String CLIENT_CREDENTIALS = "client_credentials";
    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTHORIZATION_URI = "https://accounts.spotify.com/api/token";
    private static final String BASE64_ENCODED_CLIENT_ID_CLIENT_SECRET = "MTUwNmU0OTM4NDE2NDZkYzkwMTdiOTFlZmYxNjU1ZTg6MGYzNzI0NDFjYTQyNDFiYmE1ODNhNWQzZWJhNjFmN2M=";

    Response postToken() {
        log.info("Requesting access token:");

        return RestAssured
                .given()
                    .header(AUTHORIZATION, "Basic " + BASE64_ENCODED_CLIENT_ID_CLIENT_SECRET)
                    .contentType(ContentType.URLENC)
                    .formParam(GRANT_TYPE, CLIENT_CREDENTIALS)
                .when()
                    .log().uri()
                    .log().headers()
                    .post(AUTHORIZATION_URI)
                .then()
                    .log().status()
                    .log().body()
                    .extract().response();
    }
}
