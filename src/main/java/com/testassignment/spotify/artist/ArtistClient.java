package com.testassignment.spotify.artist;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Step;

/**
 * REST Client for Artist API
 * Singleton implementation
 */
@Slf4j
enum ArtistClient {

    INSTANCE;

    private static final String ARTISTS_BASE_URI = "https://api.spotify.com/v1/artists";
    private static final String AUTHORIZATION = "Authorization";

    @Step
    Response getArtistById(String accessToken, String artistId) {
        log.info("Request artist by ID:");
        String artistByIdUri = ARTISTS_BASE_URI + "/" + artistId;

        return RestAssured
                .given()
                    .header(AUTHORIZATION, "Bearer " + accessToken)
                .when()
                    .log().uri()
                    .log().headers()
                    .get(artistByIdUri)
                .then()
                    .log().status()
                    .log().body()
                    .extract().response();
    }
}
