package com.testassignment.spotify.artist;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

public class ArtistService {

    private final ArtistClient artistClient = ArtistClient.INSTANCE;

    @Step
    public Response requestArtistById(String accessToken, String artistId) {
        return artistClient.getArtistById(accessToken, artistId);
    }
}
