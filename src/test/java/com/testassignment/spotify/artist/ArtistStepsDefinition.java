package com.testassignment.spotify.artist;

import com.testassignment.spotify.context.ScenarioContextUtils;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import org.apache.hc.core5.http.HttpStatus;

import static com.testassignment.spotify.artist.ArtistTestContextEnum.ARTIST_BY_ID_RESPONSE;
import static com.testassignment.spotify.common.CommonTestContextEnum.ACCESS_TOKEN;
import static org.assertj.core.api.Assertions.assertThat;

public class ArtistStepsDefinition {

    @Steps
    private ArtistService artistService;

    @When("client request artist by existing/non-existing {string} ID")
    public void requestArtistById(String artistId) {
        String accessToken = ScenarioContextUtils.getFromContext(ACCESS_TOKEN);

        Response artistByIdResponse = artistService.requestArtistById(accessToken, artistId);

        ScenarioContextUtils.addToContext(ARTIST_BY_ID_RESPONSE, artistByIdResponse);
    }

    @Then("client receive artist with {string} name")
    public void clientReceiveArtistWithExpectedName(String expectedArtistName) {
        Response artistByIdResponse = ScenarioContextUtils.getFromContext(ARTIST_BY_ID_RESPONSE);

        int statusCode = artistByIdResponse.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.SC_OK);

        String actualArtistName = artistByIdResponse.getBody().path("name");
        assertThat(actualArtistName).isEqualTo(expectedArtistName);
    }

    @Then("client receive Bad Request error instead of artist")
    public void clientReceiveBadRequestErrorInsteadOfArtist() {
        Response artistByIdResponse = ScenarioContextUtils.getFromContext(ARTIST_BY_ID_RESPONSE);

        int statusCode = artistByIdResponse.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.SC_BAD_REQUEST);
    }
}
