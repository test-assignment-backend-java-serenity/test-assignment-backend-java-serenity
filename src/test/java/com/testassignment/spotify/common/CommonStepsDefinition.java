package com.testassignment.spotify.common;

import com.testassignment.spotify.authorize.AuthorizeService;
import io.cucumber.java.en.Given;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static com.testassignment.spotify.common.CommonTestContextEnum.ACCESS_TOKEN;

public class CommonStepsDefinition {

    @Steps
    private AuthorizeService authorizeService;

    @Given("client is authorized")
    public void authorize() {
        String accessToken = authorizeService.requestAccessToken();

        Serenity.setSessionVariable(ACCESS_TOKEN).to(accessToken);
    }
}