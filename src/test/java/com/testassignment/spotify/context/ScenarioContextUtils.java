package com.testassignment.spotify.context;

import lombok.experimental.UtilityClass;
import net.serenitybdd.core.Serenity;

/**
 * Utility class for simplification work with Serenity context
 */
@UtilityClass
public class ScenarioContextUtils {

    public void addToContext(Object key, Object value) {
        Serenity.setSessionVariable(key).to(value);
    }

    public <T> T getFromContext(Object key) {
        return Serenity.sessionVariableCalled(key);
    }
}
