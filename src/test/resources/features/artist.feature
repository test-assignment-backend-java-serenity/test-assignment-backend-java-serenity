Feature: Artist REST API

  Scenario: Request artist by ID
    Given client is authorized
    When client request artist by existing "7Ln80lUS6He07XvHI8qqHH" ID
    Then client receive artist with "Arctic Monkeys" name

  Scenario: Request artist by non-existing ID and get Bad Request error
    Given client is authorized
    When client request artist by non-existing "A7A7A7A7A7A7A7A7A7A7" ID
    Then client receive Bad Request error instead of artist
